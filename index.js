const fastify = require('fastify')({
  logger: true,
});

fastify.get('/api/hello', async function (req, res) {
  try {

    const getRandomNumberBetween = (min, max) => {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }
    const randomNumber = getRandomNumberBetween(0,50);

    if(randomNumber > 20) {
      throw new Error("😡 something bad is happening");
    }

    res
      .header('Content-Type', 'application/json; charset=utf-8')
      .send({message: "Hello World", result: randomNumber});

  } catch (err) {
    console.log(err)
    fastify.log.error(err);
    res.code(error.response.code).send(err.response.body);
  }
});

fastify.listen(8080, '0.0.0.0', (err, address) => {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }

  fastify.log.info(`server listening on ${address}`);
});
